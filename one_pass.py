from shared import count_trailing_zeroes


def one_pass_solution(A):
    mins = [[-1 for _ in range(len(A))] for _ in range(len(A))]
    # fill out the first row
    mins[0][0] = A[0][0]
    for col in range(1, len(A)):
        mins[0][col] = mins[0][col - 1] * A[0][col]
    for row in range(1, len(A)):
        mins[row][0] = mins[row - 1][0] * A[row][0]
        for col in range(1, len(A)):
            above_product = mins[row - 1][col] * A[row][col]
            left_product = mins[row][col - 1] * A[row][col]
            if count_trailing_zeroes(above_product) < count_trailing_zeroes(left_product):
                mins[row][col] = above_product
            else:
                mins[row][col] = left_product
    return count_trailing_zeroes(mins[-1][-1])
