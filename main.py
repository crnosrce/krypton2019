# Matrix A, consisting of N rows and N columns of non-negative integers, is given. Rows are numbered from 0 to N−1 (from top to bottom). Columns are numbered from 0 to N−1 (from left to right). We would like to find a path that starts at the upper-left corner (0, 0) and, moving only right and down, finishes at the bottom-right corner (N−1, N−1). Then, all the numbers on this path will be multiplied together.
#
# Find a path such that the product of all the numbers on the path contain the minimal number of trailing zeros. We assume that 0 has 1 trailing zero.
#
# Write a function:
#
# def solution(A)
#
# that, given matrix A, returns the minimal number of trailing zeros.
#
# Examples:
#
# 1. Given matrix A below:
#
# The picture describes the first example test.
#
# the function should return 1. The optimal path is: (0,0) → (0,1) → (0,2) → (1,2) → (2,2) → (2,3) → (3,3). The product of numbers 2, 10, 1, 4, 2, 1, 1 is 160, which has one trailing zero. There is no path that yields a product with no trailing zeros.
#
# 2. Given matrix A below:
#
# The picture describes the second example test.
#
# the function should return 2. One of the optimal paths is: (0,0) → (1,0) → (1,1) → (1,2) → (2,2) → (3,2) → (3,3). The product of numbers 10, 1, 1, 1, 10, 1, 1 is 100, which has two trailing zeros. There is no path that yields a product with fewer than two trailing zeros.
#
# 3. Given matrix A below:
#
# The picture describes the third example test.
#
# the function should return 1. One of the optimal paths is: (0,0) → (0,1) → (1,1) → (1,2) → (2,2). The product of numbers 10, 10, 0, 10, 10 is 0, which has one trailing zero. There is no path that yields a product with no trailing zeros.
#
# Write an efficient algorithm for the following assumptions:
#
# N is an integer within the range [1..500];
# each element of matrix A is an integer within the range [0..1,000,000,000].

import random
import cProfile
import pstats

from brute_force import brute_force_solution
from one_pass import one_pass_solution
from one_pass_factorise import one_pass_factorise_solution
from shared import generate_random


def run_hard(vals, function_to_profile):
    for _ in range(10):
        function_to_profile(vals)


def profile(vals, function_to_profile):
    print('Array size = {}x{}'.format(len(vals[0]), len(vals[0])))
    pr = cProfile.Profile()
    pr.enable()
    run_hard(vals, function_to_profile)
    pr.disable()
    pstats.Stats(pr).sort_stats('time', 'cumulative').print_stats()


def main():
    random.seed(129)  # so random
    functions_to_profile = [brute_force_solution]
    for function_to_profile in functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing tiny array:')
        vals = generate_random(dim=6)
        profile(vals, function_to_profile)

    fast_functions_to_profile = [one_pass_solution]
    for function_to_profile in fast_functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing small-medium sized array:')
        vals = generate_random(dim=10)
        profile(vals, function_to_profile)
        print('Testing medium sized array:')
        vals = generate_random(dim=50)
        profile(vals, function_to_profile)
        print('Testing large array:')
        vals = generate_random(dim=100)
        profile(vals, function_to_profile)


    very_fast_functions_to_profile = [one_pass_factorise_solution]
    for function_to_profile in very_fast_functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing small-medium sized array:')
        vals = generate_random(dim=10)
        profile(vals, function_to_profile)
        print('Testing medium sized array:')
        vals = generate_random(dim=50)
        profile(vals, function_to_profile)
        print('Testing large array:')
        vals = generate_random(dim=100)
        profile(vals, function_to_profile)
        print('Testing max sized array:')
        vals = generate_random(dim=500)
        profile(vals, function_to_profile)

if __name__ == '__main__':
    main()
