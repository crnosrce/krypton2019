from shared import successors, count_trailing_zeroes


def generate_paths(A):
    height = len(A)
    width = len(A[0])
    current_cell = 0, 0
    final_cell = height - 1, width - 1
    paths = [[current_cell]]
    while paths[0][-1] != final_cell:
        new_paths = []
        for path in paths:
            next_cells = successors(path[-1], height - 1, width - 1)
            path_copy = path.copy()
            if len(next_cells) > 0:
                path.append(next_cells[0])
                if len(next_cells) > 1:
                    path_copy.append(next_cells[1])
                    new_paths.append(path_copy)
        paths.extend(new_paths)
    return paths


def product_generator(A, paths):
    for path in paths:
        product = 1
        for cell in path:
            product *= A[cell[0]][cell[1]]
        # print("Product: {} for Path: {}".format(product, path))
        yield product


def brute_force_solution(A):
    paths = generate_paths(A)
    min_trailing_zeroes = min(count_trailing_zeroes(product) for product in product_generator(A, paths))
    return min_trailing_zeroes