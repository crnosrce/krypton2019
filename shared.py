import random


def count_trailing_zeroes(val):
    # returns the number of trailing zeroes and the least significant non-zero digit
    if val == 0:
        return 1
    current_val = val
    one_tenth = current_val // 10
    num_trailing_zeroes = 0
    while 10 <= current_val == 10 * one_tenth:
        current_val = one_tenth
        one_tenth = current_val // 10
        num_trailing_zeroes += 1
    return num_trailing_zeroes


def successors(cell, max_height, max_width):
    successor_cells = []
    if cell[0] < max_height:
        successor_cells.append((cell[0] + 1, cell[1]))
    if cell[1] < max_width:
        successor_cells.append((cell[0], cell[1] + 1))
    return successor_cells


def generate_random(dim):
    vals = []
    for row in range(dim):
        new_row = []
        vals.append(new_row)
        for col in range(dim):
            new_row.append(random.randint(0, 1000000000))
    return vals
