import unittest
import random

from brute_force import (brute_force_solution,
                         generate_paths)
from one_pass import one_pass_solution
from one_pass_factorise import one_pass_factorise_solution
from shared import count_trailing_zeroes, generate_random


class KryptonTestCase(unittest.TestCase):

    def test_count_trailing_zeroes(self):
        vals = [0, 10, 100, 199, 5693, 569300, 1234501010]
        num_zeroes = [1, 1, 2, 0, 0, 2, 1]
        for i, val in enumerate(vals):
            self.assertEqual(num_zeroes[i], count_trailing_zeroes(val),
                             "Val: {} doesn't have {} zeroes.".format(val, num_zeroes[i]))

    def test_generate_paths(self):
        small2dArray = [[1, 2], [3, 4]]
        expected_paths = [[(0, 0), (1, 0), (1, 1)],
                          [(0, 0), (0, 1), (1, 1)]]
        self.assertEqual(expected_paths, generate_paths((small2dArray)))

        small3dArray = [[1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]]
        expected_paths = [[(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
                          [(0, 0), (0, 1), (1, 1), (2, 1), (2, 2)],
                          [(0, 0), (1, 0), (1, 1), (2, 1), (2, 2)],
                          [(0, 0), (0, 1), (0, 2), (1, 2), (2, 2)],
                          [(0, 0), (0, 1), (1, 1), (1, 2), (2, 2)],
                          [(0, 0), (1, 0), (1, 1), (1, 2), (2, 2)]]
        self.assertEqual(expected_paths, generate_paths((small3dArray)))

    def test_small(self):
        smallArray = [[0]]
        self.assertEqual(1, brute_force_solution(smallArray))

    def _test_example_1(self, function_to_test):
        vals = [[2, 10, 1, 3],
                [10, 5, 4, 5],
                [2, 10, 2, 1],
                [25, 2, 5, 1]]
        self.assertEqual(1, function_to_test(vals))

    def _test_example_2(self, function_to_test):
        vals = [[2, 10, 1, 3],
                [10, 5, 4, 5],
                [2, 10, 2, 1],
                [25, 2, 5, 1]]
        self.assertEqual(1, function_to_test(vals))

    def _test_example_3(self, function_to_test):
        vals = [[2, 10, 1, 3],
                [10, 5, 4, 5],
                [2, 10, 2, 1],
                [25, 2, 5, 1]]
        self.assertEqual(1, function_to_test(vals))

    def test_codility_example_1_brute_force(self):
        self._test_example_1(brute_force_solution)

    def test_codility_example_2_brute_force(self):
        self._test_example_2(brute_force_solution)

    def test_codility_example_3_brute_force(self):
        self._test_example_3(brute_force_solution)

    def test_codility_example_1_one_pass(self):
        self._test_example_1(one_pass_solution)

    def test_codility_example_2_one_pass(self):
        self._test_example_2(one_pass_solution)

    def test_codility_example_3_one_pass(self):
        self._test_example_3(one_pass_solution)

    def test_medium_random_one_pass(self):
        random.seed(129)  # so random
        vals = generate_random(5)
        self.assertEqual(brute_force_solution(vals), one_pass_solution(vals))

    def test_medium1_one_pass(self):
        vals = [[629317686, 932038714, 839697280, 558745649, 291840116],
                [49578360, 949584352, 979713832, 953872377, 745996405],
                [428210005, 677388951, 241271082, 353691651, 929112971],
                [880496722, 249520601, 120121629, 759962450, 588914737],
                [164451066, 146678102, 728034930, 845617179, 295461577]]
        self.assertEqual(0, one_pass_solution(vals))

    def test_codility_example_1_one_pass_factorise(self):
        self._test_example_1(one_pass_factorise_solution)

    def test_codility_example_2_one_pass_factorise(self):
        self._test_example_2(one_pass_factorise_solution)

    def test_codility_example_3_one_pass_factorise(self):
        self._test_example_3(one_pass_factorise_solution)

    def test_medium_random_one_pass_factorise(self):
        random.seed(129)  # so random
        vals = generate_random(5)
        self.assertEqual(brute_force_solution(vals), one_pass_factorise_solution(vals))

    def test_medium1_one_pass_factorise(self):
        vals = [[629317686, 932038714, 839697280, 558745649, 291840116],
                [49578360, 949584352, 979713832, 953872377, 745996405],
                [428210005, 677388951, 241271082, 353691651, 929112971],
                [880496722, 249520601, 120121629, 759962450, 588914737],
                [164451066, 146678102, 728034930, 845617179, 295461577]]
        self.assertEqual(0, one_pass_factorise_solution(vals))

    def test_tiny_with_zero_one_pass_factorise(self):
        vals = [[1, 1],
                [0, 1]]
        self.assertEqual(0, one_pass_factorise_solution(vals))


if __name__ == '__main__':
    unittest.main()
