
def factorise_2_5(val):
    if val == 0:
        # handling for zero
        return -1, -1
    else:
        # given a value, this function returns its factors of 2 and 5 as a tuple
        current_val = val
        # first sieve out the powers of 2
        half = current_val // 2
        power_2 = 0
        while half * 2 == current_val:
            current_val = half
            half //= 2
            power_2 += 1
        # now sieve out the powers of 5
        fifth = current_val // 5
        power_5 = 0
        while fifth * 5 == current_val:
            current_val = fifth
            fifth //= 5
            power_5 += 1
        return power_2, power_5


def factored_prod(lhs_factors, rhs_factors):
    if lhs_factors[0] == -1 or rhs_factors[0] == -1:
        # special "zero" value
        return -1, -1
    else:
        return lhs_factors[0] + rhs_factors[0], lhs_factors[1] + rhs_factors[1]


def factored_zeroes(factors):
    if factors[0] == -1:
        # this is a zero, which is defined to have 1 trailing zero
        return 1
    else:
        # the factors are 2**n, 5**m. Whichever of these factors is smallest
        # determines how many zeroes the product will have. For example,
        # 2**4 * 5**9 = 5**5 * 10**4, which has 4 zeroes
        return min(factors)


def one_pass_factorise_solution(A):
    mins = [[-1 for _ in range(len(A))] for _ in range(len(A))]
    # fill out the first row
    mins[0][0] = factorise_2_5(A[0][0])
    for col in range(1, len(A)):
        current_factors = factorise_2_5(A[0][col])
        mins[0][col] = factored_prod(mins[0][col - 1], current_factors)
    for row in range(1, len(A)):
        current_factors = factorise_2_5(A[row][0])
        mins[row][0] = factored_prod(mins[row - 1][0], current_factors)
        for col in range(1, len(A)):
            current_factors = factorise_2_5(A[row][col])
            above_product = factored_prod(mins[row - 1][col], current_factors)
            left_product = factored_prod(mins[row][col - 1], current_factors)
            if factored_zeroes(above_product) < factored_zeroes(left_product):
                mins[row][col] = above_product
            else:
                mins[row][col] = left_product
    return factored_zeroes(mins[-1][-1])


def solution(A):
    return one_pass_factorise_solution(A)
